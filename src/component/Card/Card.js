import React from 'react';
import './Card.css';
import {Link} from 'react-router-dom'

const Card = (props) => {

    return( <div >
        <Link to="/singlenote">
            <h5 className="CardTitle"> {props.name} </h5>
        </Link>
            <p  className="CardText"> {props.text} </p>

        </div>
    )
};
export default Card;
