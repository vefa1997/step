import React from 'react';
import './Create.css';
import Button from "../Header/Button";

const Create = (props) => {

    return( 
        <div>
               
            <div className="Form">
                <div className="CardMaker">
                    <h3>Fill Data</h3>
                    <input className="Title" type="text" />
                    <textarea rows="4" cols="50" >
                    Enter text here...</textarea>                    
                </div>
                <div className="Buttons">
                    <Button btnName="Create" className="Buttons"></Button>
                </div>
            </div>
            
        </div>
     )
};
export default Create;