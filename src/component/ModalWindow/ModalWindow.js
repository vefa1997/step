import React, { Component } from 'react';
import './ModalWindow.css';


class  ModalWindow extends Component{
    render() {

        let modal = (
            <div className="ModalStyles" >
                <div className="headerModal">
                    <span className=" headerP">Delete this note?</span>

                </div>

                <div>{this.props.children}</div>
                <a className=" buttonCancel colorWhite" onClick={this.props.onClose}>Cancel</a>
                <a className=" buttonYes colorWhite" onClick={this.props.onClose}>Yes</a>
            </div>
        );


        if (! this.props.isOpen) {
            modal = null;
        }

        return (
            <div>
                {modal}
            </div>
        );
    }
}
export default ModalWindow;
