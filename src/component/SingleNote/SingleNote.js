import React, { Component }  from 'react';
import './SingleNote.css';
import ModalWindow from "../ModalWindow/ModalWindow";




class SingleNote extends Component{
    state = {
        isOpen: false
    }
    render() {
        return (
            <div className="singleContainer">


                <div className="singleDiv">Note</div>
                <div className="singleDiv">
                    <div>
                        <button>EDIT</button>
                    </div>
                    <div>
                        <button>ARCHIVE</button>
                    </div>
                    <div>
                        <button onClick={(e) => this.setState({isOpen: true})}>DELETE</button>
                    </div>


                    <ModalWindow isOpen= {this.state.isOpen} onClose={ (e) => this.setState({isOpen: false})}>

                    </ModalWindow>
                    <div>{this.props.children}</div>


                </div>

            </div>
        );
    }
}






export default SingleNote;